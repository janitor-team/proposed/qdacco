Catalan - English Dictionary, (c) 2005, 2006, 2007, 2008, 2009, 2011, 2013, 2015, 2017
		Carles Pina i Estany <carles@pina.cat>

qdacco is a program which uses data from Dacco project. You can find Dacco
project on:

	http://www.catalandictionary.org

Dacco project is a dictionary written from scratch by Catalan-speakers
and English-speakers, 100% free (LGPL licensed).

qdacco uses Dacco data off-line, so it's fast and should be easy to install.

Copyright (C) 2005, 2006, 2007, 2008, 2009, 2011, 2013, 2015 Carles Pina i Estany

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

